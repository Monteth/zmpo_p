//
// Created by monteth on 12/29/17.
//

#ifndef ZMPO_P_TIMESTAMP_H
#define ZMPO_P_TIMESTAMP_H


#include "Date.h"

class TimeStamp : public Date {
public:
    TimeStamp(int year, int month, int day, int hour, int minute, int second);

    TimeStamp(int year, int month, int day, int hour, int minute);

    explicit TimeStamp(unsigned long seconds) : Date(seconds) {};

    int getHour();

    int getMinute();

    int getSecond();

    long howManyMinutesDiffer(TimeStamp otherStamp);

    long howManySecondsDiffer(TimeStamp otherStamp);

    void addHours(int hours);

    TimeStamp getNewStampXHoursLater(int hours);

    TimeStamp operator+(int hours);

    int operator-(TimeStamp &otherTimeStamp);
};


#endif //ZMPO_P_TIMESTAMP_H
