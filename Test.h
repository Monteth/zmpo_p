//
// Created by monteth on 12/29/17.
//

#ifndef ZMPO_P_TEST_H
#define ZMPO_P_TEST_H

void testDateDiff();

void testTimeStampAddHours();

void customTest();

void startTests();

void testDateConstruction();

void testDateAddDays();

void testDateGetters();

void testDateOperators();

void testTimeStampGetters();

void testTimeStampConstruction();

void testDate();

void testTimeStamp();

void testTimeStampOperators();

void testTimeStampDifference();

void testTimeStampAddDays();

#endif //ZMPO_P_TEST_H
