//
// Created by monteth on 12/29/17.
//

#ifndef ZMPO_P_DATE_H
#define ZMPO_P_DATE_H

#include "Tools.h"

class Date {
public:
    Date(){seconds = 0;};

    explicit Date(long seconds){this->seconds = seconds;};

    Date(int year, int month, int day);

    int diff(Date otherDate);

    long getSeconds();

    Date getNewDateXDaysLater(int days);

    void addDays(int days);

    void addSecond();

    void addMinute();

    int getYear();

    int getMonth();

    int getDay();

    bool operator==(Date otherDate);

    bool operator<=(Date otherDate);

    bool operator>=(Date otherDate);

    bool operator>(Date otherDate);

    bool operator<(Date otherDate);

    Date operator+(int days);

    int operator-(Date otherDate);

protected:
    long seconds = 0;

private:

    long getSecInThisMonth();

    long getSecInThisYear();
};


#endif //ZMPO_P_DATE_H
