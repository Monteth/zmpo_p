//
// Created by monteth on 12/29/17.
//

#ifndef ZMPO_P_TOOLS_H
#define ZMPO_P_TOOLS_H


#include "Date.h"

bool isLeap(int year);

int getNrOfLeapYears(int year);

long minToSec(long minutes);

long hrToSec(int hours);

long daysToSec(int days);





#endif //ZMPO_P_TOOLS_H
