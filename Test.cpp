//
// Created by monteth on 12/29/17.
//

#include <cassert>
#include <iostream>
#include "Test.h"
#include "Date/Date.h"
#include "Date/Values.h"
#include "Date/TimeStamp.h"




void startTests() {
    Date d1(4, 1, 1);
    d1.addDays(365);
    assert(d1.getSeconds() == Date(4, 12, 31).getSeconds());
    assert((Date(4,1,1) + 365).getSeconds() == Date(4,12,31).getSeconds());
    assert(Date(1,1,1).operator-(Date(1,1,3)) == 2);

    testDate();
    testTimeStamp();
}

void testTimeStamp() {
    testTimeStampConstruction();
    testTimeStampGetters();
    testTimeStampOperators();
    testTimeStampDifference();
    testTimeStampAddDays();
    testTimeStampAddHours();
}

void testTimeStampAddHours() {
    TimeStamp date1 = TimeStamp(1997, 1, 8, 8, 30);
    assert(date1.getNewStampXHoursLater(12).getSeconds() == TimeStamp(1997, 1, 8, 20, 30).getSeconds());
    assert(date1.getNewStampXHoursLater(-24).getSeconds() == TimeStamp(1997, 1, 7, 8, 30).getSeconds());
}

void testTimeStampAddDays() {
    TimeStamp date1 = TimeStamp(1997, 1, 8, 8, 30);
    assert(date1.getNewDateXDaysLater(10000).getSeconds() == TimeStamp(2024, 5, 26, 8, 30).getSeconds());
    assert(date1.getNewDateXDaysLater(8192).getSeconds() == TimeStamp(2019, 6, 14, 8, 30).getSeconds());
}

void testTimeStampDifference() {
    assert(TimeStamp(346, 11, 24, 23, 35).howManyMinutesDiffer(TimeStamp(346, 11, 24, 23, 35)) == 0);

    assert(TimeStamp(346, 11, 24, 23, 35).howManyMinutesDiffer(TimeStamp(2018, 1, 5, 12, 25)) == 878922050);
    //and with sec
    assert(TimeStamp(346, 11, 24, 23, 35).howManyMinutesDiffer(TimeStamp(2018, 1, 5, 12, 25, 12)) == 878922050);

    assert(TimeStamp(346, 11, 24, 23, 35).howManySecondsDiffer(TimeStamp(2018, 1, 5, 12, 25, 12)) == 52735323012);
}

void testTimeStampOperators() {
    assert(TimeStamp(346, 11, 24, 23, 35, 16) == TimeStamp(346, 11, 24, 23, 35, 16));
    assert(TimeStamp(346, 11, 24, 23, 35, 15) <= TimeStamp(346, 11, 24, 23, 35, 16));
    assert(TimeStamp(346, 11, 24, 23, 36, 16) >= TimeStamp(346, 11, 24, 23, 35, 16));
    assert(TimeStamp(346, 11, 25, 23, 35, 16) >  TimeStamp(346, 11, 24, 23, 35, 16));
    assert(TimeStamp(346, 11, 23, 23, 35, 16) <  TimeStamp(346, 11, 24, 23, 35, 16));
}

void testDate() {
    testDateConstruction();
    testDateDiff();
    testDateAddDays();
    testDateGetters();
    testDateOperators();
    customTest();
}

void testDateDiff() {
    assert(Date(2000, 1,1).diff(Date(2018,1,1)) == 6575);
}

void customTest() {
    assert(Date(1,1,1).getSeconds() == 0);
    assert(Date(1,1,1).getYear() == 1);
    assert(Date(2,1,1).getSeconds() == daysToSec(365));
    assert(Date(2,1,1).getYear() == 2);
    assert(Date(3,1,1).getSeconds() == 2 * daysToSec(365));
    assert(Date(3,1,1).getYear() == 3);
    assert(Date(4,1,1).getSeconds() == 3 * daysToSec(365));
    assert(Date(4,1,1).getYear() == 4);
    assert(Date(4,12,31).getSeconds() == 4 * daysToSec(365));
    assert(Date(4,12,31).getYear() == 4);
    assert(Date(5,1,1).getSeconds() == 4 * daysToSec(365) + daysToSec(1));
    assert(Date(5,1,1).getYear() == 5);
    assert(Date(1999, 01, 1).getYear() == 1999);
    assert(Date(1999, 12, 30).getYear() == 1999);
    assert(Date(2000, 01, 1).getYear() == 2000);
    assert(Date(2001, 01, 1).getYear() == 2001);
}

void testTimeStampConstruction() {
    assert(TimeStamp(1,1,1,235,643,234) == TimeStamp(1,1,1,23,59,59));
    assert(TimeStamp(1,1,1,235,643) == TimeStamp(1,1,1,23,60));
    assert(TimeStamp(1, 1, 1, 12, 12, 12).getSeconds() == 43932);
    assert(TimeStamp(2001, 12, 31, 12, 12).getSeconds() == 63145397520);
}

void testTimeStampGetters() {
    TimeStamp timeStamp1 = TimeStamp(2001, 12, 31, 12, 12, 12);
    assert(timeStamp1.getYear() == 2001);
    assert(timeStamp1.getMonth() == 12);
    assert(timeStamp1.getDay() == 31);
    assert(timeStamp1.getHour() == 12);
    assert(timeStamp1.getMinute() == 12);
    assert(timeStamp1.getSecond() == 12);

    TimeStamp timeStamp = TimeStamp(346, 11, 24, 23, 35, 16);
    assert(timeStamp.getYear() == 346);
    assert(timeStamp.getMonth() == 11);
    assert(timeStamp.getDay() == 24);
    assert(timeStamp.getSecond() == 16);
    assert(timeStamp.getMinute() == 35);
    assert(timeStamp.getHour() == 23);
}

void testDateOperators() {
    assert(Date(4000, 02, 29) == Date(4000, 02, 29));
    assert(Date(4000, 02, 29) <= Date(4000, 02, 29));
    assert(Date(4000, 03, 1) >= Date(4000, 02, 29));
    assert(Date(4000, 03, 1) > Date(4000, 02, 29));
    assert(Date(4000, 01, 1) < Date(4000, 02, 29));
}

void testDateGetters() {
    Date d1 = Date(4000, 02, 29);
    assert(d1.getYear() == 4000);
    assert(d1.getMonth() == 2);
    assert(d1.getDay() == 29);

    assert(Date(1, 01, 1).getYear() == 1);
    assert(Date(2, 01, 1).getYear() == 2);
    assert(Date(3, 01, 1).getYear() == 3);
    assert(Date(4, 01, 1).getYear() == 4);
    assert(Date(5, 01, 1).getYear() == 5);
}

void testDateAddDays() {
    Date date1 = Date(1, 1, 1);
    assert(date1.getNewDateXDaysLater(1).getSeconds() == daysToSec(1));

    Date date2 = Date(1, 1, 1);
    assert(date2.getNewDateXDaysLater(238796).getSeconds() == daysToSec(238796));
}

void testDateConstruction() {
    assert(Date(-435, -1, -768) == Date(1,1,1));

    assert(Date(1, 1, 1).getSeconds() == 0);

    assert(Date(1, 1, 2).getSeconds() == daysToSec(1));

    assert(Date(1, 2, 25).getSeconds() == daysToSec(NR_OF_DAYS_JAN + 24));

    assert(Date(2, 1, 1).getSeconds() == daysToSec(NR_OF_DAYS_IN_YEAR));

    assert(Date(4, 1, 1).getSeconds() == daysToSec(3 * NR_OF_DAYS_IN_YEAR ));

    assert(Date(4, 3, 1).getSeconds() == daysToSec(3 * NR_OF_DAYS_IN_YEAR + NR_OF_DAYS_JAN + NR_OF_DAYS_LEAP_FEB));

    assert(Date(5, 1, 1).getSeconds() == daysToSec(3 * NR_OF_DAYS_IN_YEAR + 1 * NR_OF_DAYS_IN_LEAP_YEAR));

    assert(Date(2001, 1, 1).getSeconds() == daysToSec(730485));

    assert(Date(2001, 12, 31).getSeconds() == daysToSec(5 * NR_OF_LEAP_IN_PERIOD * NR_OF_DAYS_IN_LEAP_YEAR + 5 * (LEAP_PERIOD - NR_OF_LEAP_IN_PERIOD) * NR_OF_DAYS_IN_YEAR + NR_OF_DAYS_IN_YEAR - 1));
}