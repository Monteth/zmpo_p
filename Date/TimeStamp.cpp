//
// Created by monteth on 12/29/17.
//

#include "TimeStamp.h"
#include "Values.h"

TimeStamp::TimeStamp(int year, int month, int day, int hour, int minute)
        : Date(year, month, day) {
    if (hour > MAX_HOUR) hour = MAX_HOUR;
    else if (hour < MIN_HOUR) hour = MIN_HOUR;
    seconds += hrToSec(hour);
    if (minute > MAX_MINUTE) minute = MAX_MINUTE;
    else if (minute < MIN_MINUTE) minute = MIN_MINUTE;
    seconds += minToSec(minute);
}

TimeStamp::TimeStamp(int year, int month, int day, int hour, int minute, int second)
        : TimeStamp(year, month, day, hour, minute) {
    if (second > MAX_SECOND) second = MAX_SECOND;
    else if (second < MIN_SECOND) second = MIN_SECOND;
    seconds += second;
}

int TimeStamp::getHour() {
    return static_cast<int>(seconds % (daysToSec(1)) / hrToSec(1));
}

int TimeStamp::getMinute() {
    return static_cast<int>(seconds % (hrToSec(1)) / minToSec(1));
}

int TimeStamp::getSecond() {
    return static_cast<int>(seconds % minToSec(1));
}

long int TimeStamp::howManyMinutesDiffer(TimeStamp otherStamp) {
    return howManySecondsDiffer(otherStamp) / minToSec(1);
}

long int TimeStamp::howManySecondsDiffer(TimeStamp otherStamp) {
    long int secDiff = seconds - otherStamp.getSeconds();
    if (secDiff < 0)
        secDiff = -secDiff;
    return secDiff;
}

void TimeStamp::addHours(int hours) {
    seconds += hrToSec(hours);
}

TimeStamp TimeStamp::getNewStampXHoursLater(int hours) {
    return TimeStamp(static_cast<unsigned long>(seconds + hrToSec(hours)));
}

TimeStamp TimeStamp::operator+(int hours){
    return getNewStampXHoursLater(hours);
}

int TimeStamp::operator-(TimeStamp &otherTimeStamp) {
    return static_cast<int>(howManyMinutesDiffer(otherTimeStamp));
}
